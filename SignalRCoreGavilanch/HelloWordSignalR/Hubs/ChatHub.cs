﻿namespace HelloWordSignalR.Hubs
{
    using Microsoft.AspNetCore.SignalR;
    using System.Threading.Tasks;

    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            //Clients sont les clients qui sont connecte dans ce ChatHub
            //All => On va lui envoyer ce message a tout le monde.

            //param1 => le nom le function Javascript que je veux executer.
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
