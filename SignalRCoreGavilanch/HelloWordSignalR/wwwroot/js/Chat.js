﻿// Initialize une connection.
// url == route definit dans le startUp
const connection = new signalR.HubConnectionBuilder().withUrl("/ChatHub").build();

// Les functions js(ReceiveMessage) que mon hub va pouvoir executer depuis c#.
connection.on("ReceiveMessage", (user, message) => {
    const msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt");
    const date = new Date().toLocaleTimeString();
    const ShowMessage = date + "<strong>" + user + "<strong> :" + msg;
    const ShowMessage2 = `${date} - ${user}: ${msg}.`;
    const li = document.createElement("li");
    li.innerHTML = ShowMessage;

    document.getElementById("messageList").appendChild(li);
});

//On commence la connection
connection.start().catch(err => console.error(err.toString()));

document.getElementById("btnSend").addEventListener("click", event => {
    const user = document.getElementById("userImput").value;
    const msg = document.getElementById("msgInput").value;

    //on invoke la function c# apres le click
    connection.invoke("SendMessage", user, msg).catch(err => console.error(err.toString()));
    event.preventDefault();
});